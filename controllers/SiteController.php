<?php

namespace app\controllers;

use app\components\MovieDb;
use app\models\Movie;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Вывод списка фильмов по заданному критерию
     *
     * @param string $type Тип выборки
     * @param int $page Страница выборки
     * @return string Рендер страницы
     */
    public function actionIndex($type = 'popularity', $page = 1)
    {
        $response = MovieDb::getMovieList($type, $page);

        $pages = new Pagination(['totalCount' => $response['total_results'] <= 1000 ? $response['total_results'] : 1000]);

        return $this->render('index', [
            'response' => $response,
            'pages' => $pages,
            'type' => $type
        ]);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Просмотр информации по выбранному фильму
     *
     * @param $id integer ID фильма
     * @return string Рендер страницы
     */
    public function actionView($id)
    {
        $model = Movie::findOne(['id' => $id]);
        if ($model == null) {
            $response = MovieDb::getMovieById($id);

            $path = realpath(Yii::getAlias('@app')) . '/web/images';
            if (!file_exists($path)) {
                mkdir($path);
            }
            file_put_contents($path . $response['poster_path'], file_get_contents('https://image.tmdb.org/t/p/w185' . $response['poster_path']));

            $model = MovieDb::addMovieToLocalDb($response);
        }

        if (Yii::$app->request->post()) {
            $rate = Yii::$app->request->post('rate');
            if (MovieDb::setRateByMovieId($id, $rate) == false) {
                Yii::$app->user->logout();
                return $this->goHome();
            }
        }

        return $this->render('view', [
            'model' => $model
        ]);
    }
}
