<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "movie_genre".
 *
 * @property integer $movie
 * @property integer $genre
 */
class MovieGenre extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'movie_genre';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['movie', 'genre'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'movie' => 'Movie',
            'genre' => 'Genre',
        ];
    }
}
