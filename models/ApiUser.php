<?php

namespace app\models;


use yii\base\Object;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\web\IdentityInterface;

class ApiUser extends Object implements IdentityInterface
{
    public $id;
    public $apikey;

    /**
     * Finds an identity by the given ID.
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        $user = Users::findOne(['guest_session_id' => $id]);
        return $user ? new static([
            'id' => $user->guest_session_id,
            'apikey' => $user->apikey
        ]) : null;
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $client = new Client(['baseUrl' => \Yii::$app->params['movieDbApiBaseUrl']]);
        $response = $client->createRequest()
            ->setUrl('/authentication/guest_session/new')
            ->addHeaders(['Accept' => 'application/json'])
            ->setData(['api_key' => $token])
            ->send();
        $response = Json::decode($response->content);

        if (isset($response['success']) && $response['success'] == true) {
            $user = new Users();
            $user->apikey = $token;
            $user->guest_session_id = $response['guest_session_id'];
            $user->save();

            return new static([
                'id' => $user->guest_session_id,
                'apikey' => $user->apikey
            ]);
        }

        return null;
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|integer an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        return $this->apikey;
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return boolean whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        return $this->apikey === $authKey;
    }
}