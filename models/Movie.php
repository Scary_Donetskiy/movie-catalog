<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "movie".
 *
 * @property integer $id
 * @property string $title
 * @property string $original_title
 * @property string $release_date
 * @property integer $runtime
 * @property string $overview
 * @property string $poster_path
 */
class Movie extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'movie';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'runtime'], 'integer'],
            [['overview'], 'string'],
            [['title', 'original_title', 'release_date', 'poster_path'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'original_title' => 'Original Title',
            'release_date' => 'Release Date',
            'runtime' => 'Runtime',
            'overview' => 'Overview',
            'poster_path' => 'Poster Path',
        ];
    }

    public function getGenres()
    {
        return $this->hasMany(Genre::className(), ['id' => 'genre'])->viaTable(MovieGenre::tableName(), ['movie' => 'id']);
    }
}
