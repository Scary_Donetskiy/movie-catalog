<?php

namespace app\components;

use Yii;
use app\models\Genre;
use app\models\Movie;
use app\models\MovieGenre;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\httpclient\Client;

/**
 * Class MovieDb
 *
 * Класс для работы с запросами TheMovieDatabase API
 *
 * @package app\components
 */
class MovieDb
{
    /**
     * Возвращает набор параметров для выборки списка фильмов по заданному критерию
     * @param $type string Тип выборки
     * @param $page integer Страница
     * @return array Набор параметров
     */
    public static function getRequestDataByType($type, $page)
    {
        switch ($type) {
            case 'popularity':
                $requestData = [
                    'api_key' => Yii::$app->user->identity->apikey,
                    'sort_by' => 'popularity.desc',
                    'language' => 'ru',
                    'page' => $page
                ];
                break;

            case 'now_in_cinema':
                $date = new \DateTime();
                $now = $date->format('Y-m-d');
                $date->modify('-2 month');
                $requestData = [
                    'api_key' => Yii::$app->user->identity->apikey,
                    'release_date.gte' => $date->format('Y-m-d'),
                    'release_date.lte' => $now,
                    'language' => 'ru',
                    'page' => $page
                ];
                break;
        }

        return $requestData;
    }

    /**
     * Возвращает список фильмов по заданному критерию
     * @param $type string Тип выборки
     * @param $page integer Номер страницы
     * @return mixed Ответ сервиса
     */
    public static function getMovieList($type, $page)
    {
        $client = new Client(['baseUrl' => \Yii::$app->params['movieDbApiBaseUrl']]);
        $response = $client->createRequest()
            ->setUrl('discover/movie')
            ->addHeaders(['Accept' => 'application/json'])
            ->setData(MovieDb::getRequestDataByType($type, $page))
            ->send();
        return Json::decode($response->content);
    }

    /**
     * Возвращает информацию о фильме по его ID
     * @param $id integer ID фильма
     * @return mixed Ответ сервиса
     */
    public static function getMovieById($id)
    {
        $client = new Client(['baseUrl' => \Yii::$app->params['movieDbApiBaseUrl']]);
        $response = $client->createRequest()
            ->setUrl('movie/' . $id)
            ->addHeaders(['Accept' => 'application/json'])
            ->setData([
                'api_key' => \Yii::$app->user->identity->apikey,
                'language' => 'ru'
            ])
            ->send();
        return Json::decode($response->content);
    }

    /**
     * Добавляет данные фильма в локальную БД
     * @param $response array Ответ сервиса
     * @return Movie Модель с фильмом
     */
    public static function addMovieToLocalDb($response)
    {
        $model = new Movie();
        $model->setAttributes($response);
        $model->save();

        foreach ($response['genres'] as $genre) {
            $genreModel = new Genre();
            $genreModel->setAttributes($genre);
            $genreModel->save();

            $movieGenre = new MovieGenre();
            $movieGenre->movie = $model->id;
            $movieGenre->genre = $genreModel->id;
            $movieGenre->save();
        }

        return $model;
    }

    /**
     * Устанавливает оценку для фильма
     * @param $id integer Id фильма
     * @param $rate integer Оценка для фильма
     * @return bool
     */
    public static function setRateByMovieId($id, $rate)
    {
        $client = new Client(['baseUrl' => \Yii::$app->params['movieDbApiBaseUrl']]);
        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl('movie/' . $id . '/rating')
            ->addHeaders(['Accept' => 'application/json'])
            ->setData([
                'api_key' => \Yii::$app->user->identity->apikey,
                'guest_session_id' => \Yii::$app->user->id,
                'value' => $rate
            ])
            ->send();
        $response = Json::decode($response->content);

        if ($response['status_code'] == 1 || $response['status_code'] == 12) {
            \Yii::$app->session->setFlash('rate', $response['status_message']);
            return true;
        } else {
            return false;
        }
    }
}