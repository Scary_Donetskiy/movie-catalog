<?php

use yii\db\Schema;
use yii\db\Migration;

class m160126_195955_movie extends Migration
{
    public function up()
    {
        $this->createTable('movie', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING,
            'original_title' => Schema::TYPE_STRING,
            'release_date' => Schema::TYPE_STRING,
            'runtime' => Schema::TYPE_INTEGER,
            'overview' => Schema::TYPE_TEXT,
            'poster_path' => Schema::TYPE_STRING
        ]);
    }

    public function down()
    {
        $this->dropTable('movie');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
