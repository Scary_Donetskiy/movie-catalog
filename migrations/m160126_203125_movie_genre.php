<?php

use yii\db\Schema;
use yii\db\Migration;

class m160126_203125_movie_genre extends Migration
{
    public function up()
    {
        $this->createTable('movie_genre', [
            'movie' => Schema::TYPE_INTEGER,
            'genre' => Schema::TYPE_INTEGER
        ]);
    }

    public function down()
    {
        $this->dropTable('movie_genre');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
