<?php

use yii\db\Schema;
use yii\db\Migration;

class m160126_200330_genre extends Migration
{
    public function up()
    {
        $this->createTable('genre', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING
        ]);
    }

    public function down()
    {
        $this->dropTable('genre');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
