<?php

use yii\db\Schema;
use yii\db\Migration;

class m160129_110620_users extends Migration
{
    public function up()
    {
        $this->createTable('users', [
            'id' => Schema::TYPE_PK,
            'apikey' => Schema::TYPE_STRING,
            'guest_session_id' => Schema::TYPE_STRING
        ]);
    }

    public function down()
    {
        $this->dropTable('users');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
