<?php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'sqlite:' . dirname(__FILE__) . '/movie_catalog.db',
    'charset' => 'utf8',
];
