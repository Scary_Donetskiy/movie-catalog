<?php

/* @var $this yii\web\View */

$this->title = $model->title . ' - Movie Catalog';
?>
<div class="site-index">
    <div class="body-content">
        <div class="row">
            <?php if(Yii::$app->session->hasFlash('rate')) : ?>
                <div class="alert alert-success" role="alert">
                    <?= Yii::$app->session->getFlash('rate') ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="row">
            <div class="col-md-2">
                <?= \yii\bootstrap\Html::img('@web/images' . $model->poster_path) ?>
            </div>
            <div class="col-md-10">
                <h1><?= "$model->title ($model->original_title)" ?></h1>
                <span class="label label-primary"><?= $model->release_date ?></span>
                <span class="label label-info">Длительность: <?= $model->runtime ?> мин.</span>
                <p><?= $model->overview ?></p>
                <?php foreach ($model->genres as $genre) : ?>
                    <span class="label label-warning"><?= $genre->name ?></span>
                <?php endforeach; ?>

                <h3>Поставить оценку</h3>
                <?= \yii\bootstrap\Html::beginForm(['', 'id' => $model->id], 'post', ['class' => 'form-inline']) ?>
                <?= \yii\bootstrap\Html::dropDownList('rate', [10], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]) ?>
                <?= \yii\bootstrap\Html::submitButton('Set Rate', ['class' => 'btn btn-primary', 'name' => 'setRate']) ?>
                <?= \yii\bootstrap\Html::endForm() ?>
            </div>
        </div>
    </div>
</div>
