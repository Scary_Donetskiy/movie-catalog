<?php

/* @var $this yii\web\View */

$this->title = 'Movie Catalog';
?>
<div class="site-index">
    <div class="body-content">
        <div class="btn-group" role="group">
            <a class="btn btn-default" href="<?= \yii\helpers\Url::toRoute(['index']) ?>" <?= $type == 'popularity' ? 'disabled' : '' ?>>Popularity (default)</a>
            <a class="btn btn-default" href="<?= \yii\helpers\Url::toRoute(['index', 'type' => 'now_in_cinema']) ?>" <?= $type == 'now_in_cinema' ? 'disabled' : '' ?>>Now in Cinema</a>
        </div>

        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>Title</th>
                <th>Release Date</th>
                <th>Detail</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($response['results'] as $item) : ?>
                <tr>
                    <th><?= $item['id'] ?></th>
                    <th><?= $item['title'] ?></th>
                    <th><?= $item['release_date'] ?></th>
                    <th><a href="<?= \yii\helpers\Url::toRoute(['view', 'id' => $item['id']]) ?>">See Detail</a></th>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

        <?= \yii\widgets\LinkPager::widget([
            'pagination' => $pages,
            'maxButtonCount' => 4
        ]) ?>
    </div>
</div>
